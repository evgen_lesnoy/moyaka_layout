import jQuery from "jquery";
import popper from "popper.js";
import bootstrap from "bootstrap";

import "./_nav";
import "./_home";
import "./_product-card";
import "./_contact";
import "./_news";
import "./_objects";
import "./_video";
import "./_equipment";
import "./_online_builder";
import "../scss/style.scss";

import { incrementValue, decrementValue } from "./_helpers";

// import { carouselNormalization } from "./_helpers";

jQuery(document).ready(function($) {
  // tooltips
  $(".image-tooltip__button-tooltip").tooltip({
    template:
      '<div class="tooltip tooltip-block tooltip-block-blue" role="tooltip"><div class="tooltip-inner"></div></div>',
    trigger: "manual"
  });

  $(".image-tooltip__button-tooltip").on("click", function() {
    let ariaDescribedby = $(this).attr("aria-describedby");

    if (!ariaDescribedby) {
      $(".image-tooltip__button-tooltip")
        .tooltip("hide")
        .removeClass("image-tooltip__button-tooltip_active");

      $(this)
        .tooltip("show")
        .addClass("image-tooltip__button-tooltip_active");
    } else {
      $(this)
        .tooltip("hide")
        .removeClass("image-tooltip__button-tooltip_active");
    }
  });

  // counter-block
  $(".counter-block").on("click", function(e) {
    e.stopPropagation();
  });

  $(".counter-block").on("click", ".counter-block_plus", function(e) {
    e.stopPropagation();
    incrementValue(e);
  });

  $(".counter-block").on("click", ".counter-block_minus", function(e) {
    e.stopPropagation();
    decrementValue(e);
  });
  // ENd counter-block

  // // slider height equal
  // carouselNormalization($("#modalTerminal .carousel-item"));
  // $("#modalTerminal").on("shown.bs.modal", function(e) {
  //   carouselNormalization($("#modalTerminal .carousel-item"));
  // });
});
