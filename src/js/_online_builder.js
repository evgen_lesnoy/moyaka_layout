import jQuery from "jquery";
import Slider from "bootstrap-slider";
import { equalHeights, iOS } from "./_helpers";

jQuery(document).ready(function($) {
  const imageArray = [".img-1-js", ".img-2-js", ".img-3-js"];

  $.each(imageArray, function(i, val) {
    equalHeights($(val));
  });

  //mob sitebar
  $(".sitebar-mob-bg").on("click", function() {
    if ($(".open-sitebar-js").hasClass("open")) {
      $(".open-sitebar-js").removeClass("open");
      $(".sitebar-mob-bg").removeClass("open");

      $("html, body").css({
        overflow: "auto"
      });

      if (iOS) {
        $("body").css({
          position: "static"
        });
      }

      $(".sitebar-col")
        .stop(true, true)
        .animate(
          {
            right: -288
          },
          500
        );
    }
  });

  $(".open-sitebar-js").on("click", function() {
    if ($(this).hasClass("open")) {
      $(this).removeClass("open");
      $(".sitebar-mob-bg").removeClass("open");

      $("html, body").css({
        overflow: "auto"
      });

      if (iOS) {
        $("body").css({
          position: "static"
        });
      }

      $(this)
        .parents(".sitebar-col")
        .stop(true, true)
        .animate(
          {
            right: -288
          },
          500
        );
    } else {
      $(this).addClass("open");
      $(".sitebar-mob-bg").addClass("open");

      $("html, body").css({
        overflow: "hidden"
      });

      if (iOS) {
        $("body").css({
          position: "fixed"
        });
      }

      $(this)
        .parents(".sitebar-col")
        .stop(true, true)
        .animate(
          {
            right: 0
          },
          500
        );
    }
  });

  // slider
  if ($("#slider-handle-js").length) {
    new Slider("#slider-handle-js", {
      tooltip: "always",
      ticks: [90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210],
      ticks_labels: [
        '<div class="slider-circle">90</div>',
        '<div class="slider-line"></div>',
        '<div class="slider-line"></div>',
        '<div class="slider-circle">120</div>',
        '<div class="slider-line"></div>',
        '<div class="slider-line"></div>',
        '<div class="slider-circle">150</div>',
        '<div class="slider-line"></div>',
        '<div class="slider-line"></div>',
        '<div class="slider-circle">180</div>',
        '<div class="slider-line"></div>',
        '<div class="slider-line"></div>',
        '<div class="slider-circle">210</div>'
      ],
      step: 10
    });
  }

  $(".entry").on("click", function() {
    let par = $(this).parents(".entry-inner");
    par.find(".entry").removeClass("active");
    $(this).addClass("active");
  });
});
