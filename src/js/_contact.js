import ymaps from "ymaps";

const apikey = "730d66e9-a5db-4307-afc5-3e0fd18bfec9";

ymaps
  .load(`https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=${apikey}`)
  .then(maps => {
    const map = new maps.Map("contactMap", {
      center: [55.7489, 37.7185],
      zoom: 17
    });

    const myGeoObject = new maps.GeoObject(
      {
        geometry: {
          type: "Point",
          coordinates: [55.7489, 37.7185]
        }
      },
      {
        preset: "islands#icon",
        iconColor: "#ff3333",
        draggable: true
      }
    );

    map.geoObjects.add(myGeoObject);
  })
  .catch(error => console.log("Failed to load Yandex Maps", error));
