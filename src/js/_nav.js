import jQuery from "jquery";
import bootstrap from "bootstrap";
import { selectpicker } from "bootstrap-select";
import { equalHeights, iOS, toggleDropdown } from "./_helpers";
import "./_home";
import "../scss/style.scss";

jQuery(document).ready(function($) {
  // select
  $(".selectpicker").selectpicker({
    width: "121px"
  });

  // menu
  // function waveDrop() {
  //   if (window.matchMedia("(max-width: 1299px)").matches) {
  //     $(".dropdown-submenu").removeClass("wave-desk-js");
  //   } else {
  //     $(".dropdown-submenu").addClass("wave-desk-js");
  //   }
  // }

  function adjustMenu() {
    let h = $(window).height() - $(".menu__navbar-row").outerHeight();
    let h_ios = 250; //100
    let pb_ios = 220; //100

    h += h_ios;
    $("#navbar-wave")
      .find(".menu__bottom-nav")
      .css({
        "padding-bottom": pb_ios
      });

    $("#navbar-wave")
      .find(".menu__container")
      .css({
        height: h
      });
  }

  // waveDrop();

  $(window).resize(function() {
    if ($("#navbar-wave").hasClass("show")) {
      adjustMenu();
    }

    // waveDrop();
  });

  // function toggleDropdown(e) {
  //   const _d = $(e.target).closest(".dropdown"),
  //     _m = $(".dropdown-menu", _d);
  //   setTimeout(
  //     function() {
  //       const shouldOpen = e.type !== "click" && _d.is(":hover");
  //       _m.toggleClass("show", shouldOpen);
  //       _d.toggleClass("show", shouldOpen);
  //       $('[data-toggle="dropdown"]', _d).attr("aria-expanded", shouldOpen);
  //     },
  //     e.type === "mouseleave" ? 300 : 0
  //   );
  // }

  $("body")
    .on("mouseenter mouseleave", ".dropdown-submenu", toggleDropdown)
    .on("click", ".dropdown-submenu .dropdown-menu a", toggleDropdown);

  // $(".dropdown-submenu").on("click mouseover", function() {
  //   console.log("1");

  //   $(this).addClass("show");

  //   $(this)
  //     .find(".dropdown-submenu__toggle")
  //     .attr("aria-expanded", "true");

  //   $(this)
  //     .find(".dropdown-menu")
  //     .addClass("show");
  //   equalHeights($(".dropdown-submenu__header-title"));
  // });

  // $(".dropdown-submenu").on("click mouseout", function() {
  //   console.log("2");
  //   $(this).removeClass("show");

  //   $(this)
  //     .find(".dropdown-submenu__toggle")
  //     .attr("aria-expanded", "false");

  //   $(this)
  //     .find(".dropdown-menu")
  //     .removeClass("show");
  // });

  $("#navbar-wave").on("show.bs.collapse", function() {
    $("html, body").css({
      overflow: "hidden"
    });

    if (iOS) {
      $("body").css({
        position: "fixed"
      });
    }

    $(".menu__phone").css({
      opacity: 0,
      "pointer-events": "none"
    });
    $(".menu__navbar-row").addClass("menu__navbar-row_active");

    adjustMenu();
  });

  $("#navbar-wave").on("hide.bs.collapse", function() {
    $("html, body").css({
      overflow: "auto"
    });

    if (iOS) {
      $("body").css({
        position: "static"
      });
    }

    $(".menu__phone").css({
      opacity: 1,
      "pointer-events": "auto"
    });
    $(".menu__navbar-row").removeClass("menu__navbar-row_active");
  });

  $(window).scroll(function(e) {
    if ($(window).scrollTop() > 0) {
      $(".menu__navbar-row").addClass("menu__navbar-row_active");
    } else {
      $(".menu__navbar-row").removeClass("menu__navbar-row_active");
    }
  });

  function shortMenu() {
    //  Для компактного меню добавить к элементу с классом dropdown-submenu класс dropdown-submenu_short
    //  Для компактного меню добавить к элементу с классом dropdown-submenu__col класс dropdown-submenu__col_short-active
    if ($(this).hasClass("show") && $(this).hasClass("dropdown-submenu")) {
      console.log("3");

      let el_active = $(this).find(".dropdown-submenu__col_short-active");
      let el = el_active.find(".dropdown-submenu__inner-shell");
      let el_give_height = el_active.find(".dropdown-submenu__shell");

      let el_heigth = el.outerHeight();
      el_give_height.height(el_heigth);

      // $(".wave-desk-js").on("mouseover", function() {
      //   let el_heigth = el.outerHeight();
      //   el_give_height.height(el_heigth);
      // });

      // $(".wave-desk-js").on("mouseout", function() {
      //   el_give_height.height("auto");
      // });
    }
  }

  $(".dropdown-submenu_short").on("click mouseover resize", shortMenu);

  $(".dropdown-submenu__col")
    .not(".dropdown-submenu__col_short-active")
    .on("click", function() {
      // $("body").off("click mouseover", ".dropdown-submenu", shortMenu);
      $(".dropdown-submenu").unbind("click mouseover", shortMenu);
      $(".dropdown-submenu__shell").height("auto");

      $(".dropdown-submenu__col").removeClass(
        "dropdown-submenu__col_short-active"
      );
      $(".dropdown-submenu").removeClass("dropdown-submenu_short");
    });

  // (function() {
  //  Для компактного меню добавить к элементу с классом dropdown-submenu класс dropdown-submenu_short
  //  Для компактного меню добавить к элементу с классом dropdown-submenu__col класс dropdown-submenu__col_short-active
  // let $dropdownSubmenuShort = $(".dropdown-submenu_short");
  // if ($dropdownSubmenuShort.length) {
  //   let el_active = $dropdownSubmenuShort.find(
  //     ".dropdown-submenu__col_short-active"
  //   );
  //   let el = el_active.find(".dropdown-submenu__inner-shell");
  //   let el_give_height = el_active.find(".dropdown-submenu__shell");
  //   $(".wave-desk-js").on("mouseover", function() {
  //     let el_heigth = el.outerHeight();
  //     el_give_height.height(el_heigth);
  //   });
  //   $(".wave-desk-js").on("mouseout", function() {
  //     el_give_height.height("auto");
  //   });
  //   $(".dropdown-submenu__col")
  //     .not(".dropdown-submenu__col_short-active")
  //     .on("click", function() {
  //       el_active.removeClass("dropdown-submenu__col_short-active");
  //       $dropdownSubmenuShort.removeClass("dropdown-submenu_short");
  //     });
  // }
  // })();
});
