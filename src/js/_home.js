import jQuery from "jquery";
import {
  checkboxCheck,
  radioCheck,
  toggleCard,
  equalHeights,
  smoothScroll,
  iOS
} from "./_helpers";

jQuery(document).ready(function($) {
  // Временно
  // $("#modalTerminal").modal({
  //   show: true
  // });

  // $(".carousel").carousel({
  //   interval: false
  // });
  // End Временно

  $.fn.checkboxRadioMethod = function(options) {
    let settings = $.extend({}, options);
    let { checkbox = {}, radio = {} } = settings;

    this.each(function() {
      let $input = $(this).find("input");
      if ($input.is(":disabled")) return true;

      switch ($input.attr("type")) {
        case "checkbox":
          {
            let callbackFunction = checkbox && checkbox.callback;
            checkboxCheck(this, callbackFunction);
          }
          break;

        case "radio":
          {
            let parent = radio && radio.parent;
            let callbackFunction = radio && radio.callback;

            radioCheck(this, parent, callbackFunction);
          }
          break;

        default:
          return true;
      }
    });

    return this;
  };

  $.fn.toggleTerminalMethod = function(options) {
    let settings = $.extend({}, options);
    let { toggleObj = {}, doubleClickObj = {}, callbackObj = {} } = settings;

    this.each(function() {
      toggleCard(this, toggleObj, doubleClickObj, callbackObj);
    });

    return this;
  };

  $.fn.equalHeightsMethod = function(options) {
    let settings = $.extend({}, options);
    let { elementsArray = [] } = settings;

    let self = $(this);

    $.each(elementsArray, function(index, value) {
      equalHeights(self.find(value));
    });

    return this;
  };

  let terminalBlockOptionEqualHeights = () => {
    $(".terminal-block__options-row .terminal-block-option").equalHeightsMethod(
      {
        elementsArray: [
          ".terminal-block-option__title",
          ".terminal-block-option__subtitle",
          ".terminal-block-option__text",
          ".terminal-block-option__price"
        ]
      }
    );
  };

  let equipmentBlockCardEqualHeights = () => {
    $(".equipment-block__row .equipment-block-card").equalHeightsMethod({
      elementsArray: [
        ".equipment-block-card__title",
        ".equipment-block-card__list",
        ".equipment-block-card__price"
      ]
    });
  };

  let equipmentFunctionCardEqualHeights = () => {
    $(
      ".equipment-block__function-row .equipment-function-card"
    ).equalHeightsMethod({
      elementsArray: [
        ".equipment-function-card__title",
        ".equipment-function-card__text",
        ".equipment-function-card__price"
      ]
    });
  };

  let vacuumCleanerCardEqualHeights = () => {
    $(
      ".vacuum-cleaners-block__inner-row .vacuum-cleaner-card"
    ).equalHeightsMethod({
      elementsArray: [
        ".vacuum-cleaner-card__title",
        ".vacuum-cleaner-card__list",
        ".vacuum-cleaner-card__price"
      ]
    });
  };

  let vacuumCleanerFunctionCardEqualHeights = () => {
    $(
      ".vacuum-cleaners-block__function-row .vacuum-cleaner-function-card"
    ).equalHeightsMethod({
      elementsArray: [
        ".vacuum-cleaner-function-card__title",
        ".vacuum-cleaner-function-card__text",
        ".vacuum-cleaner-function-card__price"
      ]
    });
  };

  let installationEquipmentBlockCardEqualHeights = () => {
    $(
      ".installation-equipment-block__row .installation-equipment-block-card"
    ).equalHeightsMethod({
      elementsArray: [
        ".installation-equipment-block-card__title",
        ".installation-equipment-block-card__list",
        ".installation-equipment-block-card__price"
      ]
    });
  };

  let terminalBigBlockOptionEqualHeights = () => {
    $(
      ".terminal-big-block__terminal-block-option-row .terminal-block-option"
    ).equalHeightsMethod({
      elementsArray: [
        ".terminal-block-option__title",
        ".terminal-block-option__subtitle",
        ".terminal-block-option__text",
        ".terminal-block-option__price"
      ]
    });
  };

  let equipmentTerminalBlockOptionEqualHeights = () => {
    $(
      ".equipment-terminal-block__terminal-block-option-row .terminal-block-option"
    ).equalHeightsMethod({
      elementsArray: [
        ".terminal-block-option__title",
        ".terminal-block-option__subtitle",
        ".terminal-block-option__text",
        ".terminal-block-option__price"
      ]
    });
  };

  let togglePostSettings = {
    toggleObj: {
      defaultElement: ".post-block__button ",
      activeClass: "post-block__button_active",
      parentElement: ".post-block__row"
    }
  };
  $(".post-block__button ").toggleTerminalMethod(togglePostSettings);

  let terminalOptionCheck = (isChecked, $elementThis) => {
    let $terminalBlockOption = $elementThis.parents(".terminal-block-option");

    if (isChecked) {
      $terminalBlockOption.addClass("terminal-block-option_active");
    } else {
      $terminalBlockOption.removeClass("terminal-block-option_active");
    }
  };
  let terminalCheckboxRadioSettings = {
    checkbox: {
      callback: terminalOptionCheck
    },
    radio: {
      callback: terminalOptionCheck,
      parent: ".terminal-block-option"
    }
  };
  $(".terminal-block-option__body").checkboxRadioMethod(
    terminalCheckboxRadioSettings
  );

  let terminalOptionFunctionsCheck = (isChecked, $elementThis) => {
    if (isChecked) {
      $elementThis.addClass("terminal-block-function_active");
    } else {
      $elementThis.removeClass("terminal-block-function_active");
    }
  };
  let terminalFunctionsCheckboxRadioSettings = {
    checkbox: {
      callback: terminalOptionFunctionsCheck
    }
  };
  $(".terminal-block-function").checkboxRadioMethod(
    terminalFunctionsCheckboxRadioSettings
  );

  // tooltips
  $(".terminal-block-function__tooltip").tooltip({
    template:
      '<div class="tooltip tooltip-block tooltip-block_gray" role="tooltip"><div class="tooltip-inner"></div></div>'
  });

  let toggleTerminalOptions = (params, $this) => {
    let {
      defaultElement,
      activeClass,
      dataAttr,
      dataAttrValue,
      parentElement
    } = params;

    let $parent = $(parentElement);
    let $allChildren = $parent.find(defaultElement);
    let $activeElement = $parent.find(`[${dataAttr}='${dataAttrValue}']`);

    $allChildren.removeClass(activeClass);
    $activeElement.addClass(activeClass);
  };
  let toggleTerminalSettings = {
    toggleObj: {
      defaultElement: ".terminals-block-card",
      activeClass: "terminals-block-card_active",
      parentElement: ".terminal-block__terminal-row"
    },
    doubleClickObj: {
      doubleClick: () =>
        $("#modalTerminal").modal({
          show: true
        })
    },
    callbackObj: {
      callback: toggleTerminalOptions,
      params: {
        defaultElement: ".terminal-block__options-card",
        activeClass: "terminal-block__options-card_active",
        dataAttr: "data-terminal-id",
        parentElement: ".terminal-block__options-container"
      },
      callbackObj: {
        callback: () => {
          terminalBlockOptionEqualHeights();
        }
      }
    }
  };
  $(".terminals-block-card").toggleTerminalMethod(toggleTerminalSettings);

  let equipmentFunctionsCheck = (isChecked, $elementThis) => {
    if (isChecked) {
      $elementThis.addClass("equipment-function-card_active");
    } else {
      $elementThis.removeClass("equipment-function-card_active");
    }
  };
  let equipmentFunctionsCheckboxRadioSettings = {
    checkbox: {
      callback: equipmentFunctionsCheck
    }
  };
  $(".equipment-function-card").checkboxRadioMethod(
    equipmentFunctionsCheckboxRadioSettings
  );

  let toggleEquipmentSettings = {
    toggleObj: {
      defaultElement: ".equipment-block-card ",
      activeClass: "equipment-block-card_active",
      parentElement: ".equipment-block__row"
    }
  };
  $(".equipment-block-card ").toggleTerminalMethod(toggleEquipmentSettings);

  let toggleButton = (params, $this) => {
    let { defaultElement, parentElement } = params;

    let $activeElement = $this.find(defaultElement);
    let $parent = $(parentElement);
    let $allChildren = $parent.find(defaultElement);

    let selectedText = $activeElement.attr("data-selected");
    let unSelectedText = $activeElement.attr("data-unselected");

    $allChildren.text(unSelectedText);
    $activeElement.text(selectedText);
  };

  let togglePurifierSettings = {
    toggleObj: {
      defaultElement: ".purifier-block-card",
      activeClass: "purifier-block-card_active",
      parentElement: ".purifiers-block__row"
    },
    callbackObj: {
      callback: toggleButton,
      params: {
        defaultElement: ".purifier-block-card__button",
        parentElement: ".purifiers-block__row"
      }
    }
  };
  $(".purifier-block-card").toggleTerminalMethod(togglePurifierSettings);

  let toggleInstallationEquipmentSettings = {
    toggleObj: {
      defaultElement: ".installation-equipment-block-card",
      activeClass: "installation-equipment-block-card_active",
      parentElement: ".installation-equipment-block__row"
    },
    callbackObj: {
      callback: toggleButton,
      params: {
        defaultElement: ".installation-equipment-block-card__button",
        parentElement: ".installation-equipment-block__row"
      }
    }
  };
  $(".installation-equipment-block-card").toggleTerminalMethod(
    toggleInstallationEquipmentSettings
  );

  $(".checkbox-item").checkboxRadioMethod();

  let vacuumCleanerFunctionsCheck = (isChecked, $elementThis) => {
    if (isChecked) {
      $elementThis.addClass("vacuum-cleaner-function-card_active");
    } else {
      $elementThis.removeClass("vacuum-cleaner-function-card_active");
    }
  };
  let vacuumCleanerFunctionsCheckboxRadioSettings = {
    checkbox: {
      callback: vacuumCleanerFunctionsCheck
    }
  };
  $(".vacuum-cleaner-function-card").checkboxRadioMethod(
    vacuumCleanerFunctionsCheckboxRadioSettings
  );

  let vacuumCleanerTerminalSettings = {
    toggleObj: {
      defaultElement: ".vacuum-cleaner-card",
      activeClass: "vacuum-cleaner-card_active",
      parentElement: ".vacuum-cleaners-block__inner-row"
    },
    callbackObj: {
      callback: toggleTerminalOptions,
      params: {
        defaultElement: ".vacuum-cleaners-block__function",
        activeClass: "vacuum-cleaners-block__function_active",
        dataAttr: "data-vacuum-cleaner-id",
        parentElement: ".vacuum-cleaners-block__functions-block"
      },
      callbackObj: {
        callback: toggleButton,
        params: {
          defaultElement: ".vacuum-cleaner-card__button",
          parentElement: ".vacuum-cleaners-block__inner-row"
        },
        callbackObj: {
          callback: vacuumCleanerFunctionCardEqualHeights
        }
      }
    }
  };
  $(".vacuum-cleaner-card").toggleTerminalMethod(vacuumCleanerTerminalSettings);

  let vacuumCleanerPostSettings = {
    toggleObj: {
      defaultElement: ".vacuum-cleaners-block__button-vacuum-cleaner-type",
      activeClass: "button-vacuum-cleaner-type_active",
      parentElement: ".vacuum-cleaners-block__row"
    },
    callbackObj: {
      callback: toggleTerminalOptions,
      params: {
        defaultElement: ".vacuum-cleaners-block__posts-container",
        activeClass: "vacuum-cleaners-block__posts-container_active",
        dataAttr: "data-vacuum-cleaner-post-id",
        parentElement: ".vacuum-cleaners-block__posts-container-shell"
      }
    }
  };
  $(".vacuum-cleaners-block__button-vacuum-cleaner-type").toggleTerminalMethod(
    vacuumCleanerPostSettings
  );

  let vacuumCleanerButtonVoltSettings = {
    toggleObj: {
      defaultElement: ".vacuum-cleaners-block__button-volt button",
      activeClass: "button-volt_active",
      parentElement: ".vacuum-cleaners-block__row"
    },
    callbackObj: {
      callback: toggleTerminalOptions,
      params: {
        defaultElement: ".vacuum-cleaners-block__post",
        activeClass: "vacuum-cleaners-block__post_active",
        dataAttr: "data-vacuum-cleaner-button-volt-id",
        parentElement: ".vacuum-cleaners-block__posts-container"
      }
    }
  };
  $(".vacuum-cleaners-block__button-volt button").toggleTerminalMethod(
    vacuumCleanerButtonVoltSettings
  );

  let toggleCaluculatorEqualHeights = () => {
    terminalBlockOptionEqualHeights();
    equipmentBlockCardEqualHeights();
    equipmentFunctionCardEqualHeights();
    vacuumCleanerCardEqualHeights();
    vacuumCleanerFunctionCardEqualHeights();
    installationEquipmentBlockCardEqualHeights();
    terminalBigBlockOptionEqualHeights();
    equipmentTerminalBlockOptionEqualHeights();
  };

  toggleCaluculatorEqualHeights();

  let calculatorSettings = {
    toggleObj: {
      defaultElement: ".home-page__content-head-button-type",
      activeClass: "button-type_active",
      parentElement: ".row"
    },
    callbackObj: {
      callback: toggleTerminalOptions,
      params: {
        defaultElement: ".home-page__calculator-type",
        activeClass: "home-page__calculator-type_active",
        dataAttr: "data-calculator-id",
        parentElement: ".container"
      },
      callbackObj: {
        callback: toggleCaluculatorEqualHeights,
        params: {}
      }
    }
  };
  $(".home-page__content-head-button-type").toggleTerminalMethod(
    calculatorSettings
  );

  $(".home-page__header-button-constructor").on("click", function(e) {
    e.preventDefault();
    smoothScroll({ $this: $(this) });
  });

  $(".vacuum-cleaners-block__next-stap").on("click", function(e) {
    e.preventDefault();
    smoothScroll({ $this: $(this) });
  });

  // sidebar
  $(".home-page__background").on("click", function() {
    if (
      $(".sidebar-calculator__toggle").hasClass(
        "sidebar-calculator__toggle_active"
      )
    ) {
      $(".sidebar-calculator__toggle").removeClass(
        "sidebar-calculator__toggle_active"
      );
      $(".home-page__background").removeClass("home-page__background_active");

      $("html, body").css({
        overflow: "auto"
      });

      if (iOS) {
        $("body").css({
          position: "static"
        });
      }

      $(".sidebar-calculator")
        .stop(true, true)
        .animate(
          {
            right: -288
          },
          500
        );
    }
  });

  $(".sidebar-calculator__toggle").on("click", function() {
    if ($(this).hasClass("sidebar-calculator__toggle_active")) {
      $(this).removeClass("sidebar-calculator__toggle_active");
      $(".home-page__background").removeClass("home-page__background_active");

      $("html, body").css({
        overflow: "auto"
      });

      if (iOS) {
        $("body").css({
          position: "static"
        });
      }

      $(this)
        .parents(".sidebar-calculator")
        .stop(true, true)
        .animate(
          {
            right: -288
          },
          500
        );
    } else {
      $(this).addClass("sidebar-calculator__toggle_active");
      $(".home-page__background").addClass("home-page__background_active");

      $("html, body").css({
        overflow: "hidden"
      });

      if (iOS) {
        $("body").css({
          position: "fixed"
        });
      }

      $(this)
        .parents(".sidebar-calculator")
        .stop(true, true)
        .animate(
          {
            right: 0
          },
          500
        );
    }
  });
});
