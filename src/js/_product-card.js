import jQuery from "jquery";
import { equalHeights } from "./_helpers";

jQuery(document).ready(function($) {
  // slider height equal
  equalHeights($("#carouselProductSlider .carousel-item > div"));

  // close tooltip after slide transition
  $("#carouselProductSlider").on("slide.bs.carousel", function() {
    $(".image-tooltip__button-tooltip")
      .tooltip("hide")
      .removeClass("image-tooltip__button-tooltip_active");
  });

  // Временно
  // $("#modalFeedback").modal({
  //   show: true
  // });

  // $(".carousel").carousel({
  //   interval: false
  // });
  // End Временно
});
