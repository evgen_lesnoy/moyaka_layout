import jQuery from "jquery";
import { cutTextDots } from "./_helpers";

jQuery(document).ready(function($) {
  $.fn.cutText = function(options) {
    // let settings = $.extend({}, options);

    this.each(function() {
      cutTextDots({ $element: $(this), length: 200 });
    });

    return this;
  };

  $(".news-card__text").cutText();
});
