import jQuery from "jquery";

jQuery(document).ready(function($) {
  const apiKey = "AIzaSyDYwPzLevXauI-kTSVXTLroLyHEONuF9Rw";

  const getYoutubeInfo = {
    key: apiKey,
    maxResults: "1",
    part: "snippet, contentDetails",
    fields:
      "items(id,snippet(title, thumbnails(medium)), contentDetails(duration))"
  };

  function getResults(videoThis, item) {
    let videoID = item.id;
    let title = item.snippet.title;
    let thumb = item.snippet.thumbnails.medium.url;
    let duration = convert_time(item.contentDetails.duration);
    let url = "https://youtube.com/watch?v=" + videoID;

    let output =
      '<div class="video-card__image-container">' +
      "<img src=" +
      thumb +
      ' alt="" class="video-card__image"> ' +
      '<p class="video-card__duration">' +
      duration +
      "</p>" +
      "</div>" +
      '<h3 class="video-card__title">' +
      CutText(title, 58) +
      "</h3>";

    videoThis.attr("href", url);

    videoThis.append(output);
  }

  function convert_time(duration) {
    let a = duration.match(/\d+/g);

    if (
      duration.indexOf("M") >= 0 &&
      duration.indexOf("H") == -1 &&
      duration.indexOf("S") == -1
    ) {
      a = [0, a[0], 0];
    }

    if (duration.indexOf("H") >= 0 && duration.indexOf("M") == -1) {
      a = [a[0], 0, a[1]];
    }
    if (
      duration.indexOf("H") >= 0 &&
      duration.indexOf("M") == -1 &&
      duration.indexOf("S") == -1
    ) {
      a = [a[0], 0, 0];
    }

    duration = 0;

    if (a.length == 3) {
      duration = duration + parseInt(a[0]) * 3600;
      duration = duration + parseInt(a[1]) * 60;
      duration = duration + parseInt(a[2]);
    }

    if (a.length == 2) {
      duration = duration + parseInt(a[0]) * 60;
      duration = duration + parseInt(a[1]);
    }

    if (a.length == 1) {
      duration = duration + parseInt(a[0]);
    }
    let h = Math.floor(duration / 3600);
    let m = Math.floor((duration % 3600) / 60);
    let s = Math.floor((duration % 3600) % 60);
    return (
      (h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") +
      m +
      ":" +
      (s < 10 ? "0" : "") +
      s
    );
  }

  function youtubeQuery(videoThis, videoId) {
    $.ajax({
      type: "GET",
      url: "https://www.googleapis.com/youtube/v3/videos?id=" + videoId,
      data: getYoutubeInfo,
      dataType: "json",
      success: function(data) {
        if (data.items.length > 0) {
          getResults(videoThis, data.items[0]);
        }
      }
    });
  }

  function CutText(str, num) {
    if (str.length < num) {
      return str;
    }

    return str.substr(0, num) + "...";
  }

  $.fn.makeVideoCard = function(options) {
    this.each(function() {
      let videoThis = $(this);
      let videoId = videoThis.attr("id");
      youtubeQuery(videoThis, videoId);
    });

    return this;
  };

  $(".video-card").makeVideoCard();
});
