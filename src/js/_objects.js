import jQuery from "jquery";
import { equalHeights } from "./_helpers";

jQuery(document).ready(function($) {
  $.fn.equalHeightsMethod = function(options) {
    let settings = $.extend({}, options);
    let { elementsArray = [] } = settings;

    let self = $(this);

    $.each(elementsArray, function(index, value) {
      equalHeights(self.find(value));
    });

    return this;
  };

  let eobjectCardEqualHeights = () => {
    $(".objects-page__cards-block .object-card").equalHeightsMethod({
      elementsArray: [
        ".object-card__title",
        ".object-card__address",
        ".object-card__subtitle",
        ".object-card__text"
      ]
    });
  };

  eobjectCardEqualHeights();
});
