import jQuery from "jquery";
import { equalHeights } from "./_helpers";

jQuery(document).ready(function($) {
  $.fn.equalHeightsMethod = function(options) {
    let settings = $.extend({}, options);
    let { elementsArray = [] } = settings;

    let self = $(this);

    $.each(elementsArray, function(index, value) {
      equalHeights(self.find(value));
    });

    return this;
  };

  let equipmentCardEqualHeights = () => {
    $(".equipment-page__cards-block .equipment-card").equalHeightsMethod({
      elementsArray: [
        ".equipment-card__title",
        ".equipment-card__price",
        ".equipment-card__list"
      ]
    });
  };

  equipmentCardEqualHeights();
});
