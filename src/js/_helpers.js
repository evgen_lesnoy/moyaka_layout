import $ from "jquery";

export const iOS =
  /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

export const checkboxCheck = (elenent, callback = undefined) => {
  $(elenent).on("click", function() {
    let $checkboxInput = $(this).find("input");
    let $checkboxBlock = $(this).find(".checkbox-block");
    let isChecked = $checkboxInput.is(":checked");

    if (isChecked) {
      $checkboxInput.prop("checked", false);
      $checkboxBlock.removeClass("checkbox-block_active");
    } else {
      $checkboxInput.prop("checked", true);
      $checkboxBlock.addClass("checkbox-block_active");
    }

    if (typeof callback === "function") {
      callback(!isChecked, $(this));
    }
  });
};

export const radioCheck = (elenent, parent, callback = undefined) => {
  $(elenent).on("click", function() {
    let $radioInput = $(this).find("input");
    let $radioBlock = $(this).find(".radio-block");
    let $parentBlock = $(this).parents(parent);
    let $allRadioInputs = $parentBlock.find('input[type="radio"]');
    let $allRadioBlocks = $allRadioInputs.parents(".radio-block");
    let isChecked = $radioInput.is(":checked");

    if (!isChecked) {
      $allRadioInputs.prop("checked", false);
      $allRadioBlocks.removeClass("radio-block_active");

      $radioInput.prop("checked", true);
      $radioBlock.addClass("radio-block_active");

      if (typeof callback === "function") {
        callback(!isChecked, $(this));
      }
    }
  });
};

export const toggleCard = (
  elenent,
  toggleObj = {},
  doubleClickObj = {},
  callbackObj = {}
) => {
  $(elenent).on("click", function() {
    let $this = $(this);
    {
      let { defaultElement, activeClass, parentElement } = toggleObj;
      let $parent = $this.parents(parentElement);
      let $allChildren = $parent.find(defaultElement);

      if (!$this.hasClass(activeClass)) {
        $allChildren.removeClass(activeClass);
        $this.addClass(activeClass);
      } else {
        {
          // Double click
          if (!$.isEmptyObject(doubleClickObj)) {
            let { doubleClick: doubleClickFunction } = doubleClickObj;

            if (typeof doubleClickFunction === "function") {
              doubleClickFunction();
            }
          }
        }
      }
    }

    {
      if (!$.isEmptyObject(callbackObj)) {
        function iterate(obj, $this) {
          for (let property in callbackObj) {
            if (typeof obj[property] == "object") {
              iterate(obj[property], $this);
            } else {
              if (typeof obj[property] == "function") {
                obj[property](obj["params"], $this);
              }
            }
          }
        }

        let dataAttr = callbackObj.params && callbackObj.params.dataAttr;

        if (typeof dataAttr != "undefined") {
          let dataAttrValue = { dataAttrValue: $this.attr(dataAttr) };

          callbackObj = {
            ...callbackObj,
            params: {
              ...callbackObj.params,
              ...dataAttrValue
            }
          };
        }

        iterate(callbackObj, $this);
      }
    }
  });
};

export const equalHeights = items => {
  var heights = [], //create empty array to store height values
    tallest; //create variable to make note of the tallest slide

  if (items.length) {
    function normalizeHeights() {
      items.each(function() {
        $(this).css("min-height", "0"); //reset min-height
        //add heights to array
        heights.push($(this).outerHeight());
      });
      tallest = Math.max.apply(null, heights); //cache largest value
      items.each(function() {
        $(this).css("min-height", tallest + "px");
      });
    }
    normalizeHeights();

    $(window).on("resize orientationchange", function() {
      (tallest = 0), (heights.length = 0); //reset vars
      items.each(function() {
        $(this).css("min-height", "0"); //reset min-height
      });
      normalizeHeights(); //run it again
    });
  }
};

export const cutTextDots = ({ $element, length, returnText = false }) => {
  let str = $element.html();
  str = str.substr(0, length) + "...";

  if (returnText) {
    return str;
  } else {
    $element.html(str);
  }
};

export const smoothScroll = ({ $this }) => {
  let scrollTo = $this.attr("href");
  $("html, body").animate({ scrollTop: $(scrollTo).offset().top + "px" });
  return false;
};

export const toggleDropdown = e => {
  const _d = $(e.target).closest(".dropdown"),
    _m = $(".dropdown-menu", _d);
  setTimeout(
    function() {
      const shouldOpen = e.type !== "click" && _d.is(":hover");
      _m.toggleClass("show", shouldOpen);
      _d.toggleClass("show", shouldOpen);
      $('[data-toggle="dropdown"]', _d).attr("aria-expanded", shouldOpen);
    },
    e.type === "mouseleave" ? 300 : 0
  );
};

export const incrementValue = e => {
  e.preventDefault();
  let $parent = $(e.target).closest(".counter-block");
  let $input = $parent.find(".counter-block__count");
  let currentVal = parseInt($input.val(), 10);

  if (!isNaN(currentVal)) {
    $input.val(currentVal + 1);
  } else {
    $input.val(0);
  }
};

export const decrementValue = e => {
  e.preventDefault();
  let $parent = $(e.target).closest(".counter-block");
  let $input = $parent.find(".counter-block__count");
  let currentVal = parseInt($input.val(), 10);

  if (!isNaN(currentVal) && currentVal > 0) {
    $input.val(currentVal - 1);
  } else {
    $input.val(0);
  }
};

// export const equalHeights = selector => {
//   var maxHeight = 0;
//   function calcEqualHeight() {
//     var el = $(this);
//     maxHeight = el.height() > maxHeight ? el.height() : maxHeight;
//   }
//   selector.each(calcEqualHeight).height(maxHeight);
// };

// export const equalHeights = ({$parent, $elenent, elementsArray}) => {
//   var maxHeight = 0;
//   function calcEqualHeight() {
//     var el = $(this);
//     maxHeight = el.height() > maxHeight ? el.height() : maxHeight;
//   }
//   selector.each(calcEqualHeight).height(maxHeight);
// };
